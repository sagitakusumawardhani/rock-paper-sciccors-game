// Get  to DOM elements
const gameContainer = document.querySelector(".container"),
  player1Result = document.querySelector(".player1_result img"),
  comResult = document.querySelector(".com_result img"),
  result = document.querySelector(".result"),
  optionImg = document.querySelectorAll(".option_img");

// Loop through each option image element
optionImg.forEach((image, index) => {
  image.addEventListener("click", (event) => {
    image.classList.add("active");

    player1Result.src = comResult.src = "assets/rock.png";
    result.textContent = "WAIT";
    result.style.backgroundColor = "#9A6F59"

    // Loop through each option image again
    optionImg.forEach((image2, index2) => {
      // If the current index doesn't match the clicked index
      // Remove the "active" class from the other option images
      index !== index2 && image2.classList.remove("active");
    });

    gameContainer.classList.add("start");

    // Set a timeout to delay the result calculation
    let time = setTimeout(() => {
      gameContainer.classList.remove("start");

      // Get the source of the clicked option image
      let imgSrc = event.target.querySelector(".option_img img");
      
      // Generate a random number between 0 and 2
      let randomNumber = Math.floor(Math.random() * 3);
      // Create an array of Computer image options
      let comImg = [
        "assets/rock.png",
        "assets/paper.png",
        "assets/scissors.png",
      ];
      // Set the Computer image to a random option from the array
      comResult.src = comImg[randomNumber];

      // Assign a letter value to the Computer option (R for rock, P for paper, S for scissors)
      let comValue = ["R", "P", "S"][randomNumber];
      // Assign a letter value to the clicked option (based on index)
      let player1Value = ["R", "P", "S"][index];

      // Set the player 1 image to the clicked option image
      if (player1Value === "R") {
        player1Result.src = "assets/rock.png"
      } else if (player1Value === "P") {
        player1Result.src = "assets/paper.png"
      } else if (player1Value === "S") {
        player1Result.src = "assets/scissors.png"
      }

      // Create an object with all possible outcomes
      let gameResult = {
        RR: "DRAW",
        RP: "YOU LOSE",
        RS: "YOU WIN",
        PP: "DRAW",
        PR: "YOU WIN",
        PS: "YOU LOSE",
        SS: "DRAW",
        SR: "YOU LOSE",
        SP: "YOU WIN",
      };

      console.log(player1Value)
      // Display the result
      result.textContent = gameResult[player1Value + comValue];

      if (result.textContent === "YOU WIN") {
        result.style.backgroundColor = ' #4C9654'
      } else if (result.textContent === "YOU LOSE") {
        result.style.backgroundColor = '#BA1A1A';
      } else if (result.textContent === "DRAW") {
        result.style.backgroundColor = '#F9B23D';
      }

    }, 2500);
  });
});